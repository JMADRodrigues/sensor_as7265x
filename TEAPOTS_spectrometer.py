# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'TEAPOTS_spectrometer.ui'
##
## Created by: Qt User Interface Compiler version 6.7.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QComboBox, QLabel, QLineEdit,
    QMainWindow, QMenuBar, QPushButton, QSizePolicy,
    QStatusBar, QWidget)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1370, 818)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.startButton = QPushButton(self.centralwidget)
        self.startButton.setObjectName(u"startButton")
        self.startButton.setGeometry(QRect(20, 130, 61, 24))
        self.stopButton = QPushButton(self.centralwidget)
        self.stopButton.setObjectName(u"stopButton")
        self.stopButton.setGeometry(QRect(90, 130, 61, 24))
        self.filenamelineEdit = QLineEdit(self.centralwidget)
        self.filenamelineEdit.setObjectName(u"filenamelineEdit")
        self.filenamelineEdit.setGeometry(QRect(20, 30, 113, 22))
        self.gainComboBox = QComboBox(self.centralwidget)
        self.gainComboBox.setObjectName(u"gainComboBox")
        self.gainComboBox.setGeometry(QRect(20, 200, 111, 22))
        self.gainComboBox.setToolTipDuration(-1)
        self.gainComboBox.setMinimumContentsLength(4)
        self.IntegrationComboBox = QComboBox(self.centralwidget)
        self.IntegrationComboBox.setObjectName(u"IntegrationComboBox")
        self.IntegrationComboBox.setGeometry(QRect(20, 250, 111, 22))
        self.IntegrationComboBox.setMinimumContentsLength(4)
        self.Spectrometerwidget = QWidget(self.centralwidget)
        self.Spectrometerwidget.setObjectName(u"Spectrometerwidget")
        self.Spectrometerwidget.setGeometry(QRect(180, 40, 721, 151))
        self.Spectrometerwidget_2 = QWidget(self.centralwidget)
        self.Spectrometerwidget_2.setObjectName(u"Spectrometerwidget_2")
        self.Spectrometerwidget_2.setGeometry(QRect(180, 220, 721, 161))
        self.Spectrometerwidget_3 = QWidget(self.centralwidget)
        self.Spectrometerwidget_3.setObjectName(u"Spectrometerwidget_3")
        self.Spectrometerwidget_3.setGeometry(QRect(180, 410, 721, 161))
        self.label = QLabel(self.centralwidget)
        self.label.setObjectName(u"label")
        self.label.setGeometry(QRect(20, 180, 49, 16))
        self.label_2 = QLabel(self.centralwidget)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setGeometry(QRect(20, 230, 111, 16))
        self.label_3 = QLabel(self.centralwidget)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setGeometry(QRect(180, 20, 151, 16))
        self.label_4 = QLabel(self.centralwidget)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setGeometry(QRect(180, 200, 151, 16))
        self.label_5 = QLabel(self.centralwidget)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setGeometry(QRect(180, 390, 151, 16))
        self.COMComboBox = QComboBox(self.centralwidget)
        self.COMComboBox.setObjectName(u"COMComboBox")
        self.COMComboBox.setGeometry(QRect(20, 100, 81, 22))
        self.COMComboBox.setMinimumContentsLength(4)
        self.label_7 = QLabel(self.centralwidget)
        self.label_7.setObjectName(u"label_7")
        self.label_7.setGeometry(QRect(20, 80, 49, 16))
        self.refreshCOMButton = QPushButton(self.centralwidget)
        self.refreshCOMButton.setObjectName(u"refreshCOMButton")
        self.refreshCOMButton.setGeometry(QRect(110, 100, 41, 24))
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 1370, 22))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.startButton.setText(QCoreApplication.translate("MainWindow", u"Start", None))
        self.stopButton.setText(QCoreApplication.translate("MainWindow", u"Stop", None))
        self.filenamelineEdit.setPlaceholderText(QCoreApplication.translate("MainWindow", u"filename.txt", None))
        self.gainComboBox.setCurrentText("")
        self.gainComboBox.setPlaceholderText("")
        self.IntegrationComboBox.setCurrentText("")
        self.IntegrationComboBox.setPlaceholderText("")
        self.label.setText(QCoreApplication.translate("MainWindow", u"GAIN", None))
        self.label_2.setText(QCoreApplication.translate("MainWindow", u"Integration Cycles", None))
        self.label_3.setText(QCoreApplication.translate("MainWindow", u"Top Sensor Data", None))
        self.label_4.setText(QCoreApplication.translate("MainWindow", u"Downward Sensor Data", None))
        self.label_5.setText(QCoreApplication.translate("MainWindow", u"Normalized Data", None))
        self.COMComboBox.setCurrentText("")
        self.COMComboBox.setPlaceholderText("")
        self.label_7.setText(QCoreApplication.translate("MainWindow", u"COM", None))
        self.refreshCOMButton.setText(QCoreApplication.translate("MainWindow", u"R", None))
    # retranslateUi


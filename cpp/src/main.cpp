#include <Arduino.h>
#include "SparkFun_Weather_Meter_Kit_Arduino_Library.h"
#include "SparkFun_AS3935.h"
#include "SparkFunBME280.h"
#include <SPI.h>
// #include <Wire.h>
#include <ESP32Time.h>
#include "FS.h"
#include "SD.h"
#include "Adafruit_VEML7700.h"
#include "SparkFun_AS7265X.h"

#define uS_TO_S_FACTOR 1000000  /* Conversion factor for micro seconds to seconds */
#define TIME_TO_SLEEP  5        /* Time ESP32 will go to sleep (in seconds) 900sec = 15min */
//#define BUTTON_PIN_BITMASK 0x1000 // GPIO12 selection for external int

// Pins for Weather Carrier with ESP32 Processor Board
int windDirectionPin = 35;
int windSpeedPin = 14;
int rainfallPin = 27;


int soilPin = 34;//A0 Declare a variable for the soil moisture sensor 
int soilPower = G0;//G0 Variable for Soil moisture Power
int min_rain_fall = 10; //minimum amount of rain in mm to send start a full measurement
//Rather than powering the sensor through the 3.3V or 5V pins, 
//we'll use a digital pin to power the sensor. This will 
//prevent corrosion of the sensor as it sits in the soil. 
// Create an instance of the weather meter kit
int rst_AS7265X_1 = 16; //Pin to select AS7265X_1
int rst_AS7265X_2 = 17; //Pin to select AS7265X_2

Adafruit_VEML7700 veml = Adafruit_VEML7700();
ESP32Time rtc;
AS7265X AS7265X_sensor;
SFEWeatherMeterKit weatherMeterKit(windDirectionPin, windSpeedPin, rainfallPin);
BME280 mySensor;
//SparkFun_AS3935 lightning;

uint8_t gain_val = 0b11;
uint16_t integration_val = 0x20;

char SD_file[] = {"/data.txt"};


bool isHexadecimal(const String& str) {
    // Check if the string starts with "0x" or "0X"
    if (str.length() > 2 && (str.charAt(0) == '0' && (str.charAt(1) == 'x' || str.charAt(1) == 'X'))) {
        // Check if the remaining characters are valid hexadecimal digits
        for (size_t i = 2; i < str.length(); ++i) {
            if (!isxdigit(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }
    return false;
}

uint16_t hexStringToUint16(String hexString) {
    uint16_t uintValue = 0;

    // Skip the "0x" prefix
    hexString.remove(0, 2);

    // Iterate over the hexadecimal digits
    for (size_t i = 0; i < hexString.length(); ++i) {
        uintValue <<= 4; // Shift left by 4 bits

        char digit = hexString.charAt(i);

        if (digit >= '0' && digit <= '9') {
            uintValue |= (digit - '0'); // Convert ASCII digit to integer
        } else if (digit >= 'A' && digit <= 'F') {
            uintValue |= (digit - 'A' + 10); // Convert ASCII hex digit to integer
        } else if (digit >= 'a' && digit <= 'f') {
            uintValue |= (digit - 'a' + 10); // Convert ASCII hex digit to integer
        } else {
            return 0;
        }
    }

    return uintValue;
}

uint8_t binaryStringToUint8(String binaryString) {
    uint8_t uintValue = 0;

    // Iterate over the binary digits starting from the third character (skipping "0b")
    for (size_t i = 2; i < binaryString.length(); ++i) {
        uintValue <<= 1; // Shift left by one bit
        if (binaryString.charAt(i) == '1') {
            uintValue |= 1; // Set the least significant bit if it's '1'
        }
    }

    return uintValue;
}

bool isBinary(const String& str) {
    // Check if the string starts with "0b" or "0B"
    if (str.length() > 2 && (str.charAt(0) == '0' && (str.charAt(1) == 'b' || str.charAt(1) == 'B'))) {
        // Check if the remaining characters are valid binary digits
        for (size_t i = 2; i < str.length(); ++i) {
            if (str.charAt(i) != '0' && str.charAt(i) != '1') {
                return false;
            }
        }
        return true;
    }
    return false;
}

//--------------------------------------------SD CARD 
void listDir(fs::FS &fs, const char * dirname, uint8_t levels){
    Serial.printf("Listing directory: %s\n", dirname);

    File root = fs.open(dirname);
    if(!root){
        Serial.println("Failed to open directory");
        return;
    }
    if(!root.isDirectory()){
        Serial.println("Not a directory");
        return;
    }

    File file = root.openNextFile();
    while(file){
        if(file.isDirectory()){
            Serial.print("  DIR : ");
            Serial.println(file.name());
            if(levels){
                listDir(fs, file.path(), levels -1);
            }
        } else {
            Serial.print("  FILE: ");
            Serial.print(file.name());
            Serial.print("  SIZE: ");
            Serial.println(file.size());
        }
        file = root.openNextFile();
    }
}

void createDir(fs::FS &fs, const char * path){
    Serial.printf("Creating Dir: %s\n", path);
    if(fs.mkdir(path)){
        Serial.println("Dir created");
    } else {
        Serial.println("mkdir failed");
    }
}

void removeDir(fs::FS &fs, const char * path){
    Serial.printf("Removing Dir: %s\n", path);
    if(fs.rmdir(path)){
        Serial.println("Dir removed");
    } else {
        Serial.println("rmdir failed");
    }
}

void readFile(fs::FS &fs, const char * path){
    //Serial.printf("Reading file: %s\n", path);

    File file = fs.open(path);
    if(!file){
        Serial.println("Failed to open file for reading");
        return;
    }

    Serial.print("START\n");
    while(file.available()){
        Serial.write(file.read());
    }
    Serial.print("END\n");
    file.close();
}

void writeFile(fs::FS &fs, const char * path, const char * message){
    Serial.printf("Writing file: %s\n", path);

    File file = fs.open(path, FILE_WRITE);
    if(!file){
        Serial.println("Failed to open file for writing");
        return;
    }
    if(file.print(message)){
        Serial.println("File written");
    } else {
        Serial.println("Write failed");
    }
    file.close();
}

void appendFile(fs::FS &fs, const char * path, const char * message){
    //Serial.printf("Appending to file: %s\n", path);

    File file = fs.open(path, FILE_APPEND);
    if(!file){
        Serial.println("Failed to open file for appending");
        return;
    }
    if(file.print(message)){
       // Serial.println("Message appended");
    } else {
        Serial.println("Append failed");
    }
    file.close();
}

void renameFile(fs::FS &fs, const char * path1, const char * path2){
    //Serial.printf("Renaming file %s to %s\n", path1, path2);
    if (fs.rename(path1, path2)) {
       // Serial.println("File renamed");
    } else {
        Serial.println("Rename failed");
    }
}

void deleteFile(fs::FS &fs, const char * path){
    Serial.printf("Deleting file: %s\n", path);
    if(fs.remove(path)){
        Serial.println("File deleted");
    } else {
        Serial.println("Delete failed");
    }
}

void testFileIO(fs::FS &fs, const char * path){
    File file = fs.open(path);
    static uint8_t buf[512];
    size_t len = 0;
    uint32_t start = millis();
    uint32_t end = start;
    if(file){
        len = file.size();
        size_t flen = len;
        start = millis();
        while(len){
            size_t toRead = len;
            if(toRead > 512){
                toRead = 512;
            }
            file.read(buf, toRead);
            len -= toRead;
        }
        end = millis() - start;
        Serial.printf("%u bytes read for %lu ms\n", flen, end);
        file.close();
    } else {
        Serial.println("Failed to open file for reading");
    }


    file = fs.open(path, FILE_WRITE);
    if(!file){
        Serial.println("Failed to open file for writing");
        return;
    }

    size_t i;
    start = millis();
    for(i=0; i<2048; i++){
        file.write(buf, 512);
    }
    end = millis() - start;
    Serial.printf("%u bytes written for %lu ms\n", 2048 * 512, end);
    file.close();
}
//--------------------------------------------SD CARD END

int readSoil()
{   
    int val = 0;
    digitalWrite(soilPower, HIGH);//turn D7 "On"
    delay(300);//wait 300 milliseconds 
    val = analogRead(soilPin);//Read the SIG value form sensor 
    //Serial.println(val);
    digitalWrite(soilPower, LOW);//turn D7 "Off"
    return val;//send current moisture value
}

void recordSensorData(){
    String tmp;
    //tmp+=rtc.getTime("%B %d %Y %H:%M:%S")+ "\t";
    tmp+=rtc.getMonth();
    tmp+="\t";
    tmp+=rtc.getDay();
    tmp+="\t";
    tmp+=rtc.getHour();
    tmp+="\t";
    tmp+=rtc.getMinute();
    tmp+="\t";
    weatherMeterKit.getWindSpeed();
    delay(1000); // Is it needed???????????????????????????????????????
    tmp += String(weatherMeterKit.getWindSpeed()) + "\t"; 
    tmp += String(weatherMeterKit.getWindDirection()) + "\t";
    tmp += String(weatherMeterKit.getTotalRainfall()) + "\t";
    tmp += String(mySensor.readFloatHumidity()) + "\t";
    tmp += String(mySensor.readFloatPressure()) +"\t";
    tmp += String(mySensor.readTempC()) +"\t";
    tmp += String(readSoil())+"\t";
    tmp += String(veml.readALS())+"\t";
    tmp += String(veml.readWhite())+"\t";
    tmp += String(veml.readLux())+"\t";


//-----------------------------------------------------------------------------------AS7265
//----------------AS7265_2
    digitalWrite(rst_AS7265X_2,HIGH);//Set to HIGH so AS7265X_1 is selected
    unsigned long startTime = millis();
    const unsigned long delayDuration = 5000; // 5 seconds
  while ((AS7265X_sensor.begin() == false) && (millis() - startTime < delayDuration))// try to connect for 5 sec max
  {
    //Serial.println("AS7265X_1 sensor not connected");
  }
    AS7265X_sensor.setGain(0b00);
    AS7265X_sensor.setIntegrationCycles(0x81);
    AS7265X_sensor.disableIndicator(); //Turn off the blue status LED
    tmp += String(AS7265X_sensor.getTemperatureAverage())+"\t";
    AS7265X_sensor.takeMeasurements(); //This is a hard wait while all 18 channels are measured
    tmp += String(AS7265X_sensor.getCalibratedA())+"\t";  //410nm
    tmp += String(AS7265X_sensor.getCalibratedB())+"\t";  //435nm
    tmp += String(AS7265X_sensor.getCalibratedC())+"\t";  //460nm
    tmp += String(AS7265X_sensor.getCalibratedD())+"\t";  //485nm
    tmp += String(AS7265X_sensor.getCalibratedE())+"\t";  //510nm
    tmp += String(AS7265X_sensor.getCalibratedF())+"\t";  //535nm
    tmp += String(AS7265X_sensor.getCalibratedG())+"\t";  //560nm
    tmp += String(AS7265X_sensor.getCalibratedH())+"\t";  //585nm
    tmp += String(AS7265X_sensor.getCalibratedR())+"\t";  //610nm
    tmp += String(AS7265X_sensor.getCalibratedI())+"\t";  //645nm
    tmp += String(AS7265X_sensor.getCalibratedS())+"\t";  //680nm
    tmp += String(AS7265X_sensor.getCalibratedJ())+"\t";  //705nm
    tmp += String(AS7265X_sensor.getCalibratedT())+"\t";  //730nm
    tmp += String(AS7265X_sensor.getCalibratedU())+"\t";  //760nm
    tmp += String(AS7265X_sensor.getCalibratedV())+"\t";  //810nm
    tmp += String(AS7265X_sensor.getCalibratedW())+"\t";  //860nm
    tmp += String(AS7265X_sensor.getCalibratedK())+"\t";  //900nm
    tmp += String(AS7265X_sensor.getCalibratedL())+"\t";  //940nm
    //Serial.println(AS7265X_sensor.getCalibratedL());
    digitalWrite(rst_AS7265X_2,LOW);//Set to LOW so AS7265X_1 is in reset mode
    


    //----------------AS7265_1
    digitalWrite(rst_AS7265X_1,HIGH);//Set to HIGH so AS7265X_2 is selected
    //delay(1000);
    startTime = millis();
    while ((AS7265X_sensor.begin() == false) && (millis() - startTime < delayDuration))// try to connect for 5 sec max
    {
        //Serial.println("AS7265X_1 sensor not connected");
    }
    AS7265X_sensor.setGain(0b00);
    AS7265X_sensor.setIntegrationCycles(0x81);
    AS7265X_sensor.disableIndicator(); //Turn off the blue status LED
    tmp += String(AS7265X_sensor.getTemperatureAverage())+"\t";
    AS7265X_sensor.takeMeasurements(); //This is a hard wait while all 18 channels are measured
    tmp += String(AS7265X_sensor.getCalibratedA())+"\t";  //410nm
    tmp += String(AS7265X_sensor.getCalibratedB())+"\t";  //435nm
    tmp += String(AS7265X_sensor.getCalibratedC())+"\t";  //460nm
    tmp += String(AS7265X_sensor.getCalibratedD())+"\t";  //485nm
    tmp += String(AS7265X_sensor.getCalibratedE())+"\t";  //510nm
    tmp += String(AS7265X_sensor.getCalibratedF())+"\t";  //535nm
    tmp += String(AS7265X_sensor.getCalibratedG())+"\t";  //560nm
    tmp += String(AS7265X_sensor.getCalibratedH())+"\t";  //585nm
    tmp += String(AS7265X_sensor.getCalibratedR())+"\t";  //610nm
    tmp += String(AS7265X_sensor.getCalibratedI())+"\t";  //645nm
    tmp += String(AS7265X_sensor.getCalibratedS())+"\t";  //680nm
    tmp += String(AS7265X_sensor.getCalibratedJ())+"\t";  //705nm
    tmp += String(AS7265X_sensor.getCalibratedT())+"\t";  //730nm
    tmp += String(AS7265X_sensor.getCalibratedU())+"\t";  //760nm
    tmp += String(AS7265X_sensor.getCalibratedV())+"\t";  //810nm
    tmp += String(AS7265X_sensor.getCalibratedW())+"\t";  //860nm
    tmp += String(AS7265X_sensor.getCalibratedK())+"\t";  //900nm
    tmp += String(AS7265X_sensor.getCalibratedL())+"\n";  //940nm
   // Serial.println(AS7265X_sensor.getCalibratedL());
    digitalWrite(rst_AS7265X_1,LOW);//Set to LOW so AS7265X_2 is in reset mode
    //Serial.println(AS7265X_sensor.getCalibratedL());
    //delay(1000);
    
    
    appendFile(SD, SD_file,tmp.c_str()) ; // write data in the file in SD card

// Display received data 
   /* Serial.println(rtc.getTime("%B %d %Y %H:%M:%S"));
    Serial.print(F("Wind speed (kph): "));
    Serial.print(weatherMeterKit.getWindSpeed(), 1);
    Serial.print(F("\n"));
    Serial.print(F("Wind direction (degrees): "));
    Serial.print(weatherMeterKit.getWindDirection(), 1);
    Serial.print(F("\n"));
    Serial.print(F("Total rainfall (mm): "));
    Serial.println(weatherMeterKit.getTotalRainfall(), 1);
    Serial.print("Humidity (%): ");
    Serial.print(mySensor.readFloatHumidity(), 0);
    Serial.print(F("\n"));
    Serial.print("Pressure (Pa): ");
    Serial.print(mySensor.readFloatPressure(), 0);
    Serial.print(F("\n"));
    //Serial.print("Alt (m): ");
    //Serial.print(mySensor.readFloatAltitudeMeters(), 1);
    //Serial.print(mySensor.readFloatAltitudeFeet(), 1);
    //Serial.print(F("\n"));
    Serial.print("Temp (deg): ");
    Serial.print(mySensor.readTempC(), 2);
    Serial.print(F("\n"));
    Serial.print("Soil Moisture = ");    
    //get soil moisture value from the function below and print it
    Serial.print(readSoil());
    //struct tm timeinfo = rtc.getTimeStruct();
    //Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S");
    // Print data from weather meter kit
    Serial.print("raw ALS: "); Serial.println(veml.readALS());
    Serial.print("raw white: "); Serial.println(veml.readWhite());
    Serial.print("lux: "); Serial.println(veml.readLux());
    */
  

}

void print_wakeup_reason(){ // Check what interupt create a wake up
  esp_sleep_wakeup_cause_t wakeup_reason;

  wakeup_reason = esp_sleep_get_wakeup_cause(); //check what interupt woke up the esp32

  switch(wakeup_reason)
  {
    /* ///// EXTERNAL INTERUPT RESETING THE ESP
    case ESP_SLEEP_WAKEUP_EXT1 : // woke up because of ext interrupt
    Serial.println("EXT WAKE UP 2\n"); 
    ESP.restart();
    break;*/
    case ESP_SLEEP_WAKEUP_EXT0 : // wake up because of the water sensor
        //Serial.println("Water rainfall"); 
        weatherMeterKit.AddrainfallCount();
        if (weatherMeterKit.getRainfallCounts() > min_rain_fall) // if the minimum amount of water is reached, then a set of measurement is started
        { recordSensorData(); // Sensors measurement
            weatherMeterKit.resetTotalRainfall(); 
            //Serial.println("New rain fall count: ");
            //Serial.print(weatherMeterKit.getTotalRainfall(),1); 
        } //reset to 0 the amount of measure water
    break;
    case ESP_SLEEP_WAKEUP_TIMER : // wake up because of the timer
        //Serial.println("Wakeup caused by timer");
        //Serial.println(weatherMeterKit.getTotalRainfall(), 1);
        recordSensorData(); // Sensors measurement
        weatherMeterKit.resetTotalRainfall(); //reset to 0 the amount of measure water
        //Serial.println("New rain fall count: ");
        //Serial.print(weatherMeterKit.getTotalRainfall(),1);
    break;
    default : // first start of the esp32 or reset by master   
        Serial.println("time");  // request for time
        Serial.flush();

        String time_from_master;

        // Start timing
        unsigned long startTime = millis();
        const unsigned long delayDuration = 1000; // 1 seconds
        bool exitLoop = false; 
        

        while (!exitLoop && (millis() - startTime < delayDuration)) {
           // Serial.println("wait for serial");
            if (Serial.available()) {  //check if data on the serial port 
             
                // Wait for User input
                time_from_master = Serial.readString(); //read the data (time) on the serial port
                while(!strstr(time_from_master.c_str(),"\n"))
                time_from_master += Serial.readString();
                //delay(100);
                //Serial.print("tm_from_master :");
                //Serial.println(time_from_master);
                //Serial.print("after tm_from_master :");
                // Convert the string to integer
                int sec; int min; int h; int day; int mth; int y;
                sscanf(time_from_master.c_str(), "%d %d %d %d %d %d", &sec, &min, &h, &day, &mth, &y ); // convert data from string to int

                char new_file_name[20];
                sprintf(new_file_name, "/%d_%d_%d_%d.txt",mth, day, h ,min); // create a new file name with the current time

                rtc.setTime(sec, min, h, day, mth, y);  // Set the rtc time with the received time
                
                // Print the parsed values
                /*
                Serial.print("Received time: ");
                Serial.print(h);
                Serial.print(":");
                Serial.print(min);
                Serial.print(":");
                Serial.println(sec);
                Serial.print("Received date: ");
                Serial.print(day);
                Serial.print("/");
                Serial.print(mth);
                Serial.print("/");
                Serial.println(y);
                */

                readFile(SD, SD_file) ; // read SD card data and print on serial port

                //Serial.println(new_file_name);
                renameFile(SD, SD_file,new_file_name); // rename the file with the data which was sent to the serial port

                exitLoop = true;       
            }
        }
        if (!exitLoop){
            // if no serial data, esp32 might have been started for the first time or reset manually
            weatherMeterKit.resetTotalRainfall();
            //Serial.print("reset rain ");
            //Serial.printf("Wakeup was not caused by deep sleep: %d\n",wakeup_reason); 
            }
    break;
  }
}

void setup()
{
    // Begin serial
    Serial.begin(115200);

    pinMode(rst_AS7265X_1, OUTPUT);//Set rst_AS7265X_1 as an OUTPUT
    digitalWrite(rst_AS7265X_1,LOW);//Set to LOW so AS7265X_1 keeps on reset mode
    pinMode(rst_AS7265X_2, OUTPUT);//Set rst_AS7265X_2 as an OUTPUT
    digitalWrite(rst_AS7265X_2,LOW);//Set to LOW so AS7265X_2 keeps on reset mode

    // SPI.begin(18,19,23,5);

    // if(!SD.begin()){ //Change to this function to manually change CS pin
    //     Serial.println("Card Mount Failed");
    //     return;
    // }

    // uint8_t cardType = SD.cardType();

    // if(cardType == CARD_NONE){
    //     Serial.println("No SD card attached");
    //     return;
    // }
    
    // pinMode(soilPower, OUTPUT);//Set G0 as an OUTPUT
    // digitalWrite(soilPower,LOW);//Set to LOW so no power is flowing through the sensor

//-----------------------------------------------------------------------------------ATMOS SENSOR START
 // Serial.println("Reading basic values from BME280");

//   Wire.begin();

//   if (mySensor.beginI2C() == false) //Begin communication over I2C
//   {
//     //Serial.println("The sensor did not respond. Please check wiring.");
//     Serial.println("I2C fail");
//     //while(1); //Freeze
//   }

//-----------------------------------------------------------------------------------VEM77
    if (!veml.begin()) {
        Serial.println("VEM not found");
        //while (1);
    }
    veml.setLowThreshold(10000);
    veml.setHighThreshold(20000);
    veml.interruptEnable(false);

    // Expected ADC values have been defined for various platforms in the
    // library, however your platform may not be included. This code will check
    // if that's the case
#ifdef SFE_WMK_PLAFTORM_UNKNOWN
    // The platform you're using hasn't been added to the library, so the
    // expected ADC values have been calculated assuming a 10k pullup resistor
    // and a perfectly linear 16-bit ADC. Your ADC likely has a different
    // resolution, so you'll need to specify it here:
    weatherMeterKit.setADCResolutionBits(10);
    
    Serial.println(F("Unknown platform! Please edit the code with your ADC resolution!"));
    Serial.println();
#endif

//     // Begin weather meter kit
//     weatherMeterKit.begin();

//   //Print the wakeup reason for ESP32
//     print_wakeup_reason();
   
//     esp_sleep_enable_ext0_wakeup(GPIO_NUM_27, LOW);
//     esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
//     //esp_sleep_enable_ext1_wakeup(BUTTON_PIN_BITMASK,ESP_EXT1_WAKEUP_ANY_HIGH); //BUTTON_PIN_BITMASK

//     gpio_deep_sleep_hold_en(); //all digital GPIOs will remain in their current state during the deep sleep 

//     esp_deep_sleep_start(); 
    
}

void loop()
{
    if (Serial.available() > 0) {
        // If there are bytes available to read
        String receivedStr = Serial.readString(); // Read the incoming byte
        if(isHexadecimal(receivedStr)){
            integration_val = hexStringToUint16(receivedStr);
        } else if(isBinary(receivedStr)){
            gain_val = binaryStringToUint8(receivedStr);
        }
    }


    String tmp;
    tmp += String(veml.readALS())+"\t";
    tmp += String(veml.readWhite())+"\t";
    tmp += String(veml.readLux())+"\t";

    //-----------------------------------------------------------------------------------AS7265
    //----------------AS7265_2
    digitalWrite(rst_AS7265X_2,HIGH);//Set to HIGH so AS7265X_1 is selected
    unsigned long startTime = millis();
    const unsigned long delayDuration = 5000; // 5 seconds
    while ((AS7265X_sensor.begin() == false) && (millis() - startTime < delayDuration))// try to connect for 5 sec max
    {
        //Serial.println("AS7265X_1 sensor not connected");
    }
    AS7265X_sensor.setGain(gain_val);
    AS7265X_sensor.setIntegrationCycles(integration_val);
    AS7265X_sensor.disableIndicator(); //Turn off the blue status LED
    // tmp += String(AS7265X_sensor.getTemperatureAverage())+"\t";
    AS7265X_sensor.takeMeasurements(); //This is a hard wait while all 18 channels are measured
    tmp += String(AS7265X_sensor.getCalibratedA())+"\t";  //410nm
    tmp += String(AS7265X_sensor.getCalibratedB())+"\t";  //435nm
    tmp += String(AS7265X_sensor.getCalibratedC())+"\t";  //460nm
    tmp += String(AS7265X_sensor.getCalibratedD())+"\t";  //485nm
    tmp += String(AS7265X_sensor.getCalibratedE())+"\t";  //510nm
    tmp += String(AS7265X_sensor.getCalibratedF())+"\t";  //535nm
    tmp += String(AS7265X_sensor.getCalibratedG())+"\t";  //560nm
    tmp += String(AS7265X_sensor.getCalibratedH())+"\t";  //585nm
    tmp += String(AS7265X_sensor.getCalibratedR())+"\t";  //610nm
    tmp += String(AS7265X_sensor.getCalibratedI())+"\t";  //645nm
    tmp += String(AS7265X_sensor.getCalibratedS())+"\t";  //680nm
    tmp += String(AS7265X_sensor.getCalibratedJ())+"\t";  //705nm
    tmp += String(AS7265X_sensor.getCalibratedT())+"\t";  //730nm
    tmp += String(AS7265X_sensor.getCalibratedU())+"\t";  //760nm
    tmp += String(AS7265X_sensor.getCalibratedV())+"\t";  //810nm
    tmp += String(AS7265X_sensor.getCalibratedW())+"\t";  //860nm
    tmp += String(AS7265X_sensor.getCalibratedK())+"\t";  //900nm
    tmp += String(AS7265X_sensor.getCalibratedL())+"\t";  //940nm
    //Serial.println(AS7265X_sensor.getCalibratedL());
    digitalWrite(rst_AS7265X_2,LOW);//Set to LOW so AS7265X_1 is in reset mode
    

    //----------------AS7265_1
    digitalWrite(rst_AS7265X_1,HIGH);//Set to HIGH so AS7265X_2 is selected
    //delay(1000);
    startTime = millis();
    while ((AS7265X_sensor.begin() == false) && (millis() - startTime < delayDuration))// try to connect for 5 sec max
    {
        //Serial.println("AS7265X_1 sensor not connected");
    }
    AS7265X_sensor.setGain(gain_val);
    AS7265X_sensor.setIntegrationCycles(integration_val);
    AS7265X_sensor.disableIndicator(); //Turn off the blue status LED
    // tmp += String(AS7265X_sensor.getTemperatureAverage())+"\t";
    AS7265X_sensor.takeMeasurements(); //This is a hard wait while all 18 channels are measured
    tmp += String(AS7265X_sensor.getCalibratedA())+"\t";  //410nm
    tmp += String(AS7265X_sensor.getCalibratedB())+"\t";  //435nm
    tmp += String(AS7265X_sensor.getCalibratedC())+"\t";  //460nm
    tmp += String(AS7265X_sensor.getCalibratedD())+"\t";  //485nm
    tmp += String(AS7265X_sensor.getCalibratedE())+"\t";  //510nm
    tmp += String(AS7265X_sensor.getCalibratedF())+"\t";  //535nm
    tmp += String(AS7265X_sensor.getCalibratedG())+"\t";  //560nm
    tmp += String(AS7265X_sensor.getCalibratedH())+"\t";  //585nm
    tmp += String(AS7265X_sensor.getCalibratedR())+"\t";  //610nm
    tmp += String(AS7265X_sensor.getCalibratedI())+"\t";  //645nm
    tmp += String(AS7265X_sensor.getCalibratedS())+"\t";  //680nm
    tmp += String(AS7265X_sensor.getCalibratedJ())+"\t";  //705nm
    tmp += String(AS7265X_sensor.getCalibratedT())+"\t";  //730nm
    tmp += String(AS7265X_sensor.getCalibratedU())+"\t";  //760nm
    tmp += String(AS7265X_sensor.getCalibratedV())+"\t";  //810nm
    tmp += String(AS7265X_sensor.getCalibratedW())+"\t";  //860nm
    tmp += String(AS7265X_sensor.getCalibratedK())+"\t";  //900nm
    tmp += String(AS7265X_sensor.getCalibratedL())+"\t";  //940nm
   // Serial.println(AS7265X_sensor.getCalibratedL());
    digitalWrite(rst_AS7265X_1,LOW);//Set to LOW so AS7265X_2 is in reset mode
    //Serial.println(AS7265X_sensor.getCalibratedL());
    //delay(1000);
    tmp += String(gain_val)+"\t";
    tmp += String(integration_val)+"\n";

    Serial.println(tmp);
}
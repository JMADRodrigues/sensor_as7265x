import time
import datetime

import numpy as np

import sys

from PySide6.QtCore import QObject, QThread, Signal
from PySide6 import QtWidgets
from PySide6.QtWidgets import QGridLayout

import pyqtgraph as pg

from TEAPOTS_spectrometer import Ui_MainWindow

import serial.tools.list_ports

import os

def file_exists(file_path):
    return os.path.exists(file_path)


def list_serial_ports():
    all_ports = []
    ports = serial.tools.list_ports.comports()
    if ports:
        print("Available serial ports:")
        for port in ports:
            print(port.device)
            all_ports.append(port.device)
        return all_ports
    else:
        print("No serial ports found")


class SerialReader(QObject):
    data_received = Signal(bytes)

    def __init__(self, port):
        super().__init__()
        self.port = port
        self.serial_connection = serial.Serial(self.port, baudrate=115200, timeout=1)

    def run(self):
        while True:
            if self.serial_connection.in_waiting > 0:
                # data = self.serial_connection.read(self.serial_connection.in_waiting)
                data = self.serial_connection.readline().decode().strip()
                self.data_received.emit(data)

    def write(self, message):
        # Send a message
        self.serial_connection.write(message.encode())

    def close(self):
        print("closing")
        self.serial_connection.close()

class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setupUi(self)

        self.ON = False

        #edit comboBox of GAIN
        self.gainComboBox.addItem("0 (x1)")
        self.gainComboBox.addItem("1 (x3.7)")
        self.gainComboBox.addItem("2 (x16)")
        self.gainComboBox.addItem("3 (x38)")
        self.gainComboBox.setCurrentIndex(3)

        # edit comboBox of Integration Cycles
        [self.IntegrationComboBox.addItem(str(i+1)) for i in range(250)]
        self.IntegrationComboBox.setCurrentIndex(32)

        # onchange combobox
        self.gainComboBox.currentIndexChanged.connect(self.setGain)
        self.IntegrationComboBox.currentIndexChanged.connect(self.setIntegrationCycles)

        #push button Start
        # Connect the button's clicked signal to the button_clicked slot
        self.startButton.clicked.connect(self.startButtonClick)
        self.stopButton.clicked.connect(self.stopButtonClick)
        self.refreshCOMButton.clicked.connect(self.refreshCOMClick)

        #call COM to see if there is any port connected
        self.refreshCOMClick()

        #Initialize data
        self.VEML_sensor_als = np.zeros(20)
        self.VEML_sensor_white = np.zeros(20)
        self.VEML_sensor_lux = np.zeros(20)
        self.AS7265X_sensor_top = []
        self.AS7265X_sensor_bottom = []
        self.data_txt = ""

        #make the plots
        self.x = [410, 435, 460, 485, 510, 535, 560, 585, 610, 645, 680, 705, 730, 760, 810, 860, 900, 940]
        self.plotVEML()
        self.plotsSpectrumSensor1()
        self.plotsSpectrumSensor2()
        self.plotCalibratedCurve()

    def setGain(self):
        gain_ = self.gainComboBox.currentText().split(" ")[0]
        if self.ON:
            self.serial_reader.write(bin(int(gain_)))

    def setIntegrationCycles(self):
        integration_ = self.IntegrationComboBox.currentText()
        if self.ON:
            self.serial_reader.write(hex(int(integration_)))

    def startButtonClick(self):
        print("start")
        self.startSerial()
        # time.sleep(2)
        self.ON = True
        self.start_ = 1

    def stopButtonClick(self):
        print("stop")
        #save data file
        text = self.filenamelineEdit.text()
        # Get current date and time
        now = datetime.datetime.now()
        # Extract year, month, day, hour, minute, and second
        time_str = "_".join(str(t_i) for t_i in [now.year, now.month, now.day, now.hour, now.minute, now.second])
        if(len(text)>0):
            with open("data/"+time_str+"__"+text+".txt", "w") as file:
                file.write(self.data_txt)
        else:
            with open("data/"+time_str+"__"+"data.txt", "w") as file:
                file.write(self.data_txt)

        #reset arrays
        self.VEML_sensor_als = np.zeros(20)
        self.VEML_sensor_white = np.zeros(20)
        self.VEML_sensor_lux = np.zeros(20)
        self.counter_veml = 0

        self.AS7265X_sensor_top = []
        self.AS7265X_sensor_bottom = []

        #reset data_txt
        self.data_txt = ""

        #close serial connection
        self.closeSerial()

        self.ON = False

    def refreshCOMClick(self):
        #reset com combobox
        self.COMComboBox.clear()
        # edit comboBox of COM Ports
        ports_ = list_serial_ports()
        if(ports_):
            for port in ports_:
                self.COMComboBox.addItem(port)
        else:
            self.COMComboBox.addItem("NONE")

    def handle_data_received(self, data):
        #parse data received:
        parsed_data = data.split("\t")

        print(data)

        if(len(parsed_data)>30):#just to skip the errors...that I don't know what the error is
            #I am also keeping the timestamp
            self.data_txt = self.data_txt + "\n" + str(time.time()) + "\t" + data

            if self.counter_veml < 20:
                """while under 20 it keeps on increasing"""
                self.VEML_sensor_als[self.counter_veml] = float(parsed_data[0])
                self.VEML_sensor_white[self.counter_veml] = float(parsed_data[1])
                self.VEML_sensor_lux[self.counter_veml] = float(parsed_data[2])
            else:
                """sliding window"""
                self.VEML_sensor_als[:-1] = self.VEML_sensor_als[1:]
                self.VEML_sensor_als[-1] = float(parsed_data[0])
                self.VEML_sensor_white[:-1] = self.VEML_sensor_white[1:]
                self.VEML_sensor_white[-1] = float(parsed_data[1])
                self.VEML_sensor_lux[:-1] = self.VEML_sensor_lux[1:]
                self.VEML_sensor_lux[-1] = float(parsed_data[2])

            self.counter_veml += 1  # count the number of samples to update the veml plot

            self.AS7265X_sensor_top.append([float(a) for a in parsed_data[3:21]])
            self.AS7265X_sensor_bottom.append([float(a) for a in parsed_data[21:39]])

            self.curve_als.setData(x=self.x_veml, y=self.VEML_sensor_als)
            self.curve_white.setData(x=self.x_veml, y=self.VEML_sensor_white)
            self.curve_lux.setData(x=self.x_veml, y=self.VEML_sensor_lux)

            self.curve_spectrum_sensor1.setData(x=self.x, y=self.AS7265X_sensor_top[-1])
            self.scatter_spectrum_sensor1.setData(x=self.x, y=self.AS7265X_sensor_top[-1])
            self.curve_spectrum_sensor2.setData(x=self.x, y=self.AS7265X_sensor_bottom[-1])
            self.scatter_spectrum_sensor2.setData(x=self.x, y=self.AS7265X_sensor_bottom[-1])
            self.curve_spectrum_calibrated.setData(x=self.x, y=(np.array(self.AS7265X_sensor_bottom[-1]))/(np.array((self.AS7265X_sensor_top[-1]))+1e-20))
            self.scatter_spectrum_calibrated.setData(x=self.x, y=(np.array(self.AS7265X_sensor_bottom[-1]))/(np.array((self.AS7265X_sensor_top[-1]))+1e-20))


    def startSerial(self):
        # start serial communication
        selected_port = self.COMComboBox.currentText()
        # Open a serial connection
        try:
            self.serial_reader = SerialReader(selected_port)
            self.thread = QThread()
            self.serial_reader.moveToThread(self.thread)
            # Connect the data_received signal of SerialReader to a slot in MainWindow
            self.serial_reader.data_received.connect(self.handle_data_received)
            # Start the thread
            self.thread.started.connect(self.serial_reader.run)
            self.thread.start()
        except serial.SerialException as e:
            print(f"Failed to open")

    def closeSerial(self):
        try:
            if hasattr(self, 'serial_reader'):
                # Close the serial connection and wait for the thread to finish
                self.serial_reader.close()
                self.thread.quit()
                self.thread.wait()
        except serial.SerialException as e:
            print("Error occurred while closing the serial connection:", e)


    def plotsSpectrumSensor1(self):
        y = np.zeros(18)
        # Create a layout for the central widget
        self.layout_graph_sensor1 = QGridLayout()
        self.graphWidget_sensor1 = pg.PlotWidget()
        self.layout_graph_sensor1.addWidget(self.graphWidget_sensor1)
        self.graphWidget_sensor1.plot()
        # configure plot
        self.graphWidget_sensor1.setBackground("white")
        self.graphWidget_sensor1.setLabel("left", "Intensity (r.u.)")
        self.graphWidget_sensor1.setLabel("bottom", "Wavelengths (nm)")
        #
        self.graphWidget_sensor1.addLegend(offset=-1, frame=True)
        self.graphWidget_sensor1.showGrid(y=True)
        self.curve_spectrum_sensor1 = pg.PlotCurveItem(x=self.x, y=y, pen=pg.mkPen("orangered", width=2), name="Sensor 2 - TOP")
        self.scatter_spectrum_sensor1 = pg.ScatterPlotItem(x=self.x, y=y, symbol='o', pen='orangered',
                                                              brush='orangered', size=5)

        self.graphWidget_sensor1.addItem(self.curve_spectrum_sensor1)
        self.graphWidget_sensor1.addItem(self.scatter_spectrum_sensor1)
        # Enable auto-scaling for x and y axes
        self.graphWidget_sensor1.enableAutoRange()
        # associate widget graph from designer to created plot
        self.Spectrometerwidget.setLayout(self.layout_graph_sensor1)

    def plotsSpectrumSensor2(self):
        y = np.zeros(18)
        # Create a layout for the central widget
        self.layout_graph_sensor2 = QGridLayout()
        self.graphWidget_sensor2 = pg.PlotWidget()
        self.layout_graph_sensor2.addWidget(self.graphWidget_sensor2)
        self.graphWidget_sensor2.plot()
        # configure plot
        self.graphWidget_sensor2.setBackground("white")
        self.graphWidget_sensor2.setLabel("left", "Intensity (r.u.)")
        self.graphWidget_sensor2.setLabel("bottom", "Wavelengths (nm)")

        self.graphWidget_sensor2.addLegend(offset=-1, frame=True)
        self.graphWidget_sensor2.showGrid(y=True)
        self.curve_spectrum_sensor2 = pg.PlotCurveItem(x=self.x, y=y, pen=pg.mkPen("lightseagreen", width=2), name="Sensor 1 - BOTTOM")
        self.scatter_spectrum_sensor2 = pg.ScatterPlotItem(x=self.x, y=y, symbol='o', pen='lightseagreen',
                                                              brush='lightseagreen', size=5)

        self.graphWidget_sensor2.addItem(self.curve_spectrum_sensor2)
        self.graphWidget_sensor2.addItem(self.scatter_spectrum_sensor2)
        # Enable auto-scaling for x and y axes
        self.graphWidget_sensor2.enableAutoRange()
        # associate widget graph from designer to created plot
        self.Spectrometerwidget_2.setLayout(self.layout_graph_sensor2)

    def plotCalibratedCurve(self):
        y = np.zeros(18)
        # Create a layout for the central widget
        self.layout_graph_calibrated = QGridLayout()
        self.graphWidget_calibrated = pg.PlotWidget()
        self.layout_graph_calibrated.addWidget(self.graphWidget_calibrated)
        self.graphWidget_calibrated.plot()
        # configure plot
        self.graphWidget_calibrated.setBackground("white")
        self.graphWidget_calibrated.setLabel("left", "Intensity (r.u.)")
        self.graphWidget_calibrated.setLabel("bottom", "Wavelengths (nm)")
        #
        self.graphWidget_calibrated.addLegend(offset=-1, frame=True)
        self.graphWidget_calibrated.showGrid(y=True)
        self.curve_spectrum_calibrated = pg.PlotCurveItem(x=self.x, y=y, pen=pg.mkPen("darkmagenta", width=2), name="Calibrated Spectrum")
        self.scatter_spectrum_calibrated = pg.ScatterPlotItem(x=self.x, y=y, symbol='o', pen='darkmagenta', brush='darkmagenta', size=5)

        self.graphWidget_calibrated.addItem(self.curve_spectrum_calibrated)
        self.graphWidget_calibrated.addItem(self.scatter_spectrum_calibrated)
        # Enable auto-scaling for x and y axes
        self.graphWidget_calibrated.enableAutoRange()
        # associate widget graph from designer to created plot
        self.Spectrometerwidget_3.setLayout(self.layout_graph_calibrated)

    def plotVEML(self):
        self.counter_veml = 0
        self.x_veml = np.linspace(1, 20, 20)

        self.layout_graph_veml = QGridLayout()
        self.graphWidget_veml = pg.PlotWidget()
        self.layout_graph_veml.addWidget(self.graphWidget_veml)
        self.graphWidget_veml.plot()

        # configure plot
        self.graphWidget_veml.setBackground("white")
        self.graphWidget_veml.setLabel("left", "Intensity (r.u.)")
        self.graphWidget_veml.setLabel("bottom", "Samples")

        #add curves
        self.graphWidget_veml.addLegend(offset=-1, frame=True)
        self.curve_als = pg.PlotCurveItem(x=self.x_veml, y=self.VEML_sensor_als, pen=pg.mkPen("orange", width=2), name="ALS")
        self.curve_white = pg.PlotCurveItem(x=self.x_veml, y=self.VEML_sensor_white, pen=pg.mkPen("red", width=2), name="White")
        self.curve_lux = pg.PlotCurveItem(x=self.x_veml, y=self.VEML_sensor_lux, pen=pg.mkPen("green", width=2), name="Lux")
        #add curve items to plots
        self.graphWidget_veml.addItem(self.curve_als)
        self.graphWidget_veml.addItem(self.curve_white)
        self.graphWidget_veml.addItem(self.curve_lux)


        # Enable auto-scaling for x and y axes
        self.graphWidget_veml.enableAutoRange()
        # associate widget graph from designer to created plot
        #self.VEMLwidget.setLayout(self.layout_graph_veml)



if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)

    window = MainWindow()
    window.show()
    app.exec()
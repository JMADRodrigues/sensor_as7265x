import time
import datetime

import numpy as np

import sys

from PySide6.QtCore import QObject, QThread, Signal
from PySide6 import QtWidgets
from PySide6.QtWidgets import QGridLayout

import pyqtgraph as pg

from TEAPOTS_datanalysis import Ui_MainWindow

import serial.tools.list_ports

import os


class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setupUi(self)

        #add files to list
        self.file_list = os.listdir("data")
        for file_i in self.file_list:
            self.FilelistWidget.addItem(file_i)

        self.FilelistWidget.clicked.connect(self.select_name)
        self.File1_horizontalSlider.valueChanged.connect(self.update_plot)
        self.File2_horizontalSlider.valueChanged.connect(self.update_plot)


        #create plot
        self.x = [410, 435, 460, 485, 510, 535, 560, 585, 610, 645, 680, 705, 730, 760, 810, 860, 900, 940]
        self.plotsSpectrumSensor1()
        self.plotsSpectrumSensor2()
        self.plotCalibratedCurve()

        #select dual
        self.selected_ = 0
        self.filename_1 = ""
        self.filename_2 = ""

    def select_name(self):
        selected_item = self.FilelistWidget.currentItem()
        if selected_item:
            selected_name = selected_item.text()

            #alternate selection
            if(self.selected_ == 0):
                self.filename_1 = selected_name
                self.selected_ = 1
            elif(self.selected_ == 1):
                self.filename_2 = selected_name
                self.selected_ = 2
            elif(self.selected_ == 2):
                self.filename_1 = self.filename_2
                self.filename_2 = selected_name

            print("Filename 1   ", self.filename_1)
            print("Filename 2   ", self.filename_2)

            self.groupBox.setTitle("File 1 - "+self.filename_1)
            self.groupBox_2.setTitle("File 2 - "+self.filename_2)

            self.loadfile()

    def extract_data1(self, s):
        self.timestamps = s[:, 0]
        self.veml_als = s[:, 1]
        self.veml_white = s[:, 2]
        self.veml_lux = s[:, 3]

        self.sensor2_ = s[:, 4:4 + 18]
        self.sensor1_ = s[:, 22:22 + 18]
        self.gain_ = s[:, -2]
        self.integration_ = s[:, -1]


    def extract_data2(self, s):
        self.timestamps2 = s[:, 0]
        self.veml_als2 = s[:, 1]
        self.veml_white2 = s[:, 2]
        self.veml_lux2 = s[:, 3]

        self.sensor2_2 = s[:, 4:4 + 18]
        self.sensor1_2 = s[:, 22:22 + 18]
        self.gain_2 = s[:, -2]
        self.integration_2 = s[:, -1]

    def loadfile(self):
        #load file
        s1 = np.loadtxt("data/"+self.filename_1)

        self.extract_data1(s1)

        # check how many lines
        nbr_lines1 = len(s1)

        # adjust slider length
        self.File1_horizontalSlider.setMinimum(0)
        self.File1_horizontalSlider.setMaximum(nbr_lines1 - 1)
        self.File1_horizontalSlider.setValue(0)

        # update first plot
        val_ = self.File1_horizontalSlider.value()
        self.graphWidget_sensor1.addLegend(offset=-1, frame=True)
        self.curve_spectrum_sensor1.setData(x=self.x, y=self.sensor2_[val_])
        self.scatter_spectrum_sensor1.setData(x=self.x, y=self.sensor2_[val_])
        self.curve_spectrum_sensor2.setData(x=self.x, y=self.sensor1_[val_])
        self.scatter_spectrum_sensor2.setData(x=self.x, y=self.sensor1_[val_])
        self.curve_spectrum_calibrated.setData(x=self.x, y=(self.sensor1_[val_] / (self.sensor2_[val_] + 1e-20)))
        self.scatter_spectrum_calibrated.setData(x=self.x, y=(self.sensor1_[val_] / (self.sensor2_[val_] + 1e-20)))

        self.gain_lineEdit.setText(str(self.gain_[val_]))
        self.integration_lineEdit.setText(str(self.integration_[val_]))


        if(self.filename_2==""):
            pass
        else:
            s2 = np.loadtxt("data/"+self.filename_2)
            self.extract_data2(s2)
            nbr_lines2 = len(s2)
            # adjust slider length
            self.File2_horizontalSlider.setMinimum(0)
            self.File2_horizontalSlider.setMaximum(nbr_lines2 - 1)
            self.File2_horizontalSlider.setValue(0)

            # update first plot
            val_2 = self.File2_horizontalSlider.value()
            self.curve_spectrum_sensor1_2.setData(x=self.x, y=self.sensor2_2[val_2])
            self.scatter_spectrum_sensor1_2.setData(x=self.x, y=self.sensor2_2[val_2],)
            self.curve_spectrum_sensor2_2.setData(x=self.x, y=self.sensor1_2[val_2])
            self.scatter_spectrum_sensor2_2.setData(x=self.x, y=self.sensor1_2[val_2])
            self.curve_spectrum_calibrated_2.setData(x=self.x, y=(self.sensor1_2[val_2] / (self.sensor2_2[val_2] + 1e-20)))
            self.scatter_spectrum_calibrated_2.setData(x=self.x, y=(self.sensor1_2[val_2] / (self.sensor2_2[val_2] + 1e-20)))

            # set gain and integration
            self.gain_lineEdit_3.setText(str(self.gain_2[val_2]))
            self.integration_lineEdit_3.setText(str(self.integration_2[val_2]))


    def update_plot(self):
        if(self.filename_1 == ""):
            pass
        else:
            self.graphWidget_sensor1.addLegend(offset=-1, frame=True)
            val_ = self.File1_horizontalSlider.value()
            self.curve_spectrum_sensor1.setData(x=self.x, y=self.sensor2_[val_])
            self.scatter_spectrum_sensor1.setData(x=self.x, y=self.sensor2_[val_])
            self.curve_spectrum_sensor2.setData(x=self.x, y=self.sensor1_[val_])
            self.scatter_spectrum_sensor2.setData(x=self.x, y=self.sensor1_[val_])
            self.curve_spectrum_calibrated.setData(x=self.x, y=(self.sensor1_[val_] / (self.sensor2_[val_] + 1e-20)))
            self.scatter_spectrum_calibrated.setData(x=self.x, y=(self.sensor1_[val_] / (self.sensor2_[val_] + 1e-20)))

            self.gain_lineEdit.setText(str(self.gain_[val_]))
            self.integration_lineEdit.setText(str(self.integration_[val_]))

        if(self.filename_2==""):
            pass
        else:
            val_2 = self.File2_horizontalSlider.value()
            self.curve_spectrum_sensor1_2.setData(x=self.x, y=self.sensor2_2[val_2])
            self.scatter_spectrum_sensor1_2.setData(x=self.x, y=self.sensor2_2[val_2])
            self.curve_spectrum_sensor2_2.setData(x=self.x, y=self.sensor1_2[val_2])
            self.scatter_spectrum_sensor2_2.setData(x=self.x, y=self.sensor1_2[val_2])
            self.curve_spectrum_calibrated_2.setData(x=self.x, y=(self.sensor1_2[val_2] / (self.sensor2_2[val_2] + 1e-20)))
            self.scatter_spectrum_calibrated_2.setData(x=self.x, y=(self.sensor1_2[val_2] / (self.sensor2_2[val_2] + 1e-20)))

            #set gain and integration
            self.gain_lineEdit_3.setText(str(self.gain_2[val_2]))
            self.integration_lineEdit_3.setText(str(self.integration_2[val_2]))


    def plotsSpectrumSensor1(self):
        y = np.zeros(18)
        # Create a layout for the central widget
        self.layout_graph_sensor1 = QGridLayout()
        self.graphWidget_sensor1 = pg.PlotWidget()
        self.layout_graph_sensor1.addWidget(self.graphWidget_sensor1)
        self.graphWidget_sensor1.plot()
        # configure plot
        self.graphWidget_sensor1.setBackground("white")
        self.graphWidget_sensor1.setLabel("left", "Intensity (r.u.)")
        self.graphWidget_sensor1.setLabel("bottom", "Wavelengths (nm)")
        #
        self.graphWidget_sensor1.addLegend(offset=-1, frame=True)
        self.graphWidget_sensor1.showGrid(y=True)
        self.curve_spectrum_sensor1 = pg.PlotCurveItem(x=self.x, y=y, pen=pg.mkPen("orangered", width=2),
                                                       name="Sensor 2 - TOP - File 1")
        self.scatter_spectrum_sensor1 = pg.ScatterPlotItem(x=self.x, y=y, symbol='o', pen='orangered',
                                                           brush='orangered', size=5)

        self.curve_spectrum_sensor1_2 = pg.PlotCurveItem(x=self.x, y=y, pen=pg.mkPen("orange", width=2), name="Sensor 2 - TOP - File 2")
        self.scatter_spectrum_sensor1_2 = pg.ScatterPlotItem(x=self.x, y=y, symbol='o', pen='orange',
                                                           brush='orange', size=5)

        self.graphWidget_sensor1.addItem(self.curve_spectrum_sensor1)
        self.graphWidget_sensor1.addItem(self.scatter_spectrum_sensor1)
        self.graphWidget_sensor1.addItem(self.curve_spectrum_sensor1_2)
        self.graphWidget_sensor1.addItem(self.scatter_spectrum_sensor1_2)
        # Enable auto-scaling for x and y axes
        self.graphWidget_sensor1.enableAutoRange()
        # associate widget graph from designer to created plot
        self.widget.setLayout(self.layout_graph_sensor1)

    def plotsSpectrumSensor2(self):
        y = np.zeros(18)
        # Create a layout for the central widget
        self.layout_graph_sensor2 = QGridLayout()
        self.graphWidget_sensor2 = pg.PlotWidget()
        self.layout_graph_sensor2.addWidget(self.graphWidget_sensor2)
        self.graphWidget_sensor2.plot()
        # configure plot
        self.graphWidget_sensor2.setBackground("white")
        self.graphWidget_sensor2.setLabel("left", "Intensity (r.u.)")
        self.graphWidget_sensor2.setLabel("bottom", "Wavelengths (nm)")

        self.graphWidget_sensor2.addLegend(offset=-1, frame=True)
        self.graphWidget_sensor2.showGrid(y=True)
        self.curve_spectrum_sensor2 = pg.PlotCurveItem(x=self.x, y=y, pen=pg.mkPen("lightseagreen", width=2), name="Sensor 1 - Down - File 1")
        self.scatter_spectrum_sensor2 = pg.ScatterPlotItem(x=self.x, y=y, symbol='o', pen='lightseagreen',
                                                           brush='lightseagreen', size=5)

        self.curve_spectrum_sensor2_2 = pg.PlotCurveItem(x=self.x, y=y, pen=pg.mkPen("green", width=2), name="Sensor 1 - Down - File 2")
        self.scatter_spectrum_sensor2_2 = pg.ScatterPlotItem(x=self.x, y=y, symbol='o', pen='green',
                                                           brush='green', size=5)

        self.graphWidget_sensor2.addItem(self.curve_spectrum_sensor2)
        self.graphWidget_sensor2.addItem(self.scatter_spectrum_sensor2)
        self.graphWidget_sensor2.addItem(self.scatter_spectrum_sensor2_2)
        self.graphWidget_sensor2.addItem(self.curve_spectrum_sensor2_2)
        # Enable auto-scaling for x and y axes
        self.graphWidget_sensor2.enableAutoRange()
        # associate widget graph from designer to created plot
        self.widget_2.setLayout(self.layout_graph_sensor2)

    def plotCalibratedCurve(self):
        y = np.zeros(18)
        # Create a layout for the central widget
        self.layout_graph_calibrated = QGridLayout()
        self.graphWidget_calibrated = pg.PlotWidget()
        self.layout_graph_calibrated.addWidget(self.graphWidget_calibrated)
        self.graphWidget_calibrated.plot()
        # configure plot
        self.graphWidget_calibrated.setBackground("white")
        self.graphWidget_calibrated.setLabel("left", "Intensity (r.u.)")
        self.graphWidget_calibrated.setLabel("bottom", "Wavelengths (nm)")
        #
        self.graphWidget_calibrated.addLegend(offset=-1, frame=True)
        self.graphWidget_calibrated.showGrid(y=True)
        self.curve_spectrum_calibrated = pg.PlotCurveItem(x=self.x, y=y, pen=pg.mkPen("darkmagenta", width=2),
                                                          name="Calibrated Spectrum - File 1")
        self.scatter_spectrum_calibrated = pg.ScatterPlotItem(x=self.x, y=y, symbol='o', pen='darkmagenta',
                                                              brush='darkmagenta', size=5)

        self.curve_spectrum_calibrated_2 = pg.PlotCurveItem(x=self.x, y=y, pen=pg.mkPen("pink", width=2),
                                                          name="Calibrated Spectrum - File 2")
        self.scatter_spectrum_calibrated_2 = pg.ScatterPlotItem(x=self.x, y=y, symbol='o', pen='pink',
                                                              brush='pink', size=5)

        self.graphWidget_calibrated.addItem(self.curve_spectrum_calibrated)
        self.graphWidget_calibrated.addItem(self.scatter_spectrum_calibrated)
        self.graphWidget_calibrated.addItem(self.scatter_spectrum_calibrated_2)
        self.graphWidget_calibrated.addItem(self.curve_spectrum_calibrated_2)
        # Enable auto-scaling for x and y axes
        self.graphWidget_calibrated.enableAutoRange()
        # associate widget graph from designer to created plot
        self.widget_3.setLayout(self.layout_graph_calibrated)


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)

    window = MainWindow()
    window.show()
    app.exec()
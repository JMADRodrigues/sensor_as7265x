# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'TEAPOTS_datanalysis.ui'
##
## Created by: Qt User Interface Compiler version 6.7.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QGroupBox, QLabel, QLineEdit,
    QListWidget, QListWidgetItem, QMainWindow, QMenuBar,
    QSizePolicy, QSlider, QStatusBar, QWidget)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1218, 664)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.FilelistWidget = QListWidget(self.centralwidget)
        self.FilelistWidget.setObjectName(u"FilelistWidget")
        self.FilelistWidget.setGeometry(QRect(10, 60, 221, 541))
        self.label = QLabel(self.centralwidget)
        self.label.setObjectName(u"label")
        self.label.setGeometry(QRect(10, 30, 49, 16))
        self.widget = QWidget(self.centralwidget)
        self.widget.setObjectName(u"widget")
        self.widget.setGeometry(QRect(250, 60, 691, 141))
        self.File1_horizontalSlider = QSlider(self.centralwidget)
        self.File1_horizontalSlider.setObjectName(u"File1_horizontalSlider")
        self.File1_horizontalSlider.setGeometry(QRect(320, 540, 621, 22))
        self.File1_horizontalSlider.setOrientation(Qt.Orientation.Horizontal)
        self.widget_2 = QWidget(self.centralwidget)
        self.widget_2.setObjectName(u"widget_2")
        self.widget_2.setGeometry(QRect(250, 220, 691, 141))
        self.widget_3 = QWidget(self.centralwidget)
        self.widget_3.setObjectName(u"widget_3")
        self.widget_3.setGeometry(QRect(250, 380, 691, 141))
        self.File2_horizontalSlider = QSlider(self.centralwidget)
        self.File2_horizontalSlider.setObjectName(u"File2_horizontalSlider")
        self.File2_horizontalSlider.setGeometry(QRect(320, 580, 621, 22))
        self.File2_horizontalSlider.setOrientation(Qt.Orientation.Horizontal)
        self.label_2 = QLabel(self.centralwidget)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setGeometry(QRect(250, 540, 49, 16))
        self.label_3 = QLabel(self.centralwidget)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setGeometry(QRect(250, 580, 49, 16))
        self.groupBox = QGroupBox(self.centralwidget)
        self.groupBox.setObjectName(u"groupBox")
        self.groupBox.setGeometry(QRect(960, 60, 251, 111))
        self.gain_lineEdit = QLineEdit(self.groupBox)
        self.gain_lineEdit.setObjectName(u"gain_lineEdit")
        self.gain_lineEdit.setEnabled(False)
        self.gain_lineEdit.setGeometry(QRect(100, 30, 71, 22))
        self.integration_lineEdit = QLineEdit(self.groupBox)
        self.integration_lineEdit.setObjectName(u"integration_lineEdit")
        self.integration_lineEdit.setEnabled(False)
        self.integration_lineEdit.setGeometry(QRect(100, 70, 71, 22))
        self.label_4 = QLabel(self.groupBox)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setGeometry(QRect(20, 30, 31, 16))
        self.label_5 = QLabel(self.groupBox)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setGeometry(QRect(20, 70, 71, 16))
        self.groupBox_2 = QGroupBox(self.centralwidget)
        self.groupBox_2.setObjectName(u"groupBox_2")
        self.groupBox_2.setGeometry(QRect(960, 200, 251, 111))
        self.gain_lineEdit_3 = QLineEdit(self.groupBox_2)
        self.gain_lineEdit_3.setObjectName(u"gain_lineEdit_3")
        self.gain_lineEdit_3.setEnabled(False)
        self.gain_lineEdit_3.setGeometry(QRect(100, 30, 71, 22))
        self.integration_lineEdit_3 = QLineEdit(self.groupBox_2)
        self.integration_lineEdit_3.setObjectName(u"integration_lineEdit_3")
        self.integration_lineEdit_3.setEnabled(False)
        self.integration_lineEdit_3.setGeometry(QRect(100, 70, 71, 22))
        self.label_8 = QLabel(self.groupBox_2)
        self.label_8.setObjectName(u"label_8")
        self.label_8.setGeometry(QRect(20, 30, 31, 16))
        self.label_9 = QLabel(self.groupBox_2)
        self.label_9.setObjectName(u"label_9")
        self.label_9.setGeometry(QRect(20, 70, 71, 16))
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 1218, 22))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.label.setText(QCoreApplication.translate("MainWindow", u"File List", None))
        self.label_2.setText(QCoreApplication.translate("MainWindow", u"File 1", None))
        self.label_3.setText(QCoreApplication.translate("MainWindow", u"File 2", None))
        self.groupBox.setTitle(QCoreApplication.translate("MainWindow", u"Parameters File 1", None))
        self.label_4.setText(QCoreApplication.translate("MainWindow", u"Gain", None))
        self.label_5.setText(QCoreApplication.translate("MainWindow", u"Integration", None))
        self.groupBox_2.setTitle(QCoreApplication.translate("MainWindow", u"Parameters File 2", None))
        self.label_8.setText(QCoreApplication.translate("MainWindow", u"Gain", None))
        self.label_9.setText(QCoreApplication.translate("MainWindow", u"Integration", None))
    # retranslateUi


# Interface for Sensor AS7265X_ control and Live Experiment


For the correct usage of this tool, be sure to follow these steps:
1. Install Python 3.10
2. Install requirements (optionally, create a virtual environment for your project)
3. Run interface on the command line


### Install Python

In order to run this tool, you first need to install python 3.10.
For this, go to this link:
https://www.python.org/downloads/release/python-31011/
and download the windows installer (64-bit):

![img.png](img.png)

During the installation, be sure to:
1. Add pip
![img_1.png](img_1.png)

2. Add PATH to variables
![img_2.png](img_2.png)

3. Open the command line and type:  
*python --version*  
If it matches the version installed, it works

### Install requirements

After Python has been installed, you can install the requirements.

1. First, you need to clone the repository or download the project.
2. Open command line
3. go the rep directory
4. Write the following command:  
*pip install requirements.txt*

### Run the interface for data acquisition

*python main.py*

### Run the data analysis tool

*python data_analysis_tool.py*  

Regarding the data analysis tool:  
1. It will list the files you recorded, located in "data/"
2. You can click in one of the files listed and it will plot the data of the file
3. The slider will allow you to go through each of the signals recorded
4. If you click in a second file, it will open both, so you can compare signals recorded in different occasions
5. You can also click on the same file twice, and it will open the same file twice. Which means you can compare different signals recorded in the same acquisition
6. The second slide controls "file 2".
7. Besides that, the gain and integration cycles selected for each file and each signal chosen by the slider, will be displayed on the right side



















